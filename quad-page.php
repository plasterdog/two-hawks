<?php
/*
*Template Name: Quad Array
 * @package plasterdog
 */

get_header(); ?>
<?php if( get_field('hero_check_off') == 'show' ): ?>
	<div id="hero-top">
	<?php if ( get_field( 'page_hero_image' ) ): ?>		
	<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
	<?php else : ?>
	<img src="<?php echo get_stylesheet_directory_uri() ?>/images/default-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
	<?php endif; ?>
	</div>	
<?php endif; ?>

<?php if( get_field('hero_check_off') == 'hide' ): ?>
	<div id="hero-top"></div>
<?php endif; ?>	
		<div class="clear"></div>

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'plasterdog' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->

<div class="quad-page">
<ul>
<li><a href="<?php the_field('first_quad_link_target');?>"><img src="<?php echo esc_url( get_field( 'first_quad_image' ) ); ?>"/></a>
	<h2><a href="<?php the_field('first_quad_link_target');?>"><?php the_field('first_quad_title');?></a></h2>
	<p style="text-align:left; margin:0 1em;"><?php the_field('first_quad_excerpt');?></p>
</li>
<li><a href="<?php the_field('second_quad_link_target');?>"><img src="<?php echo esc_url( get_field( 'second_quad_image' ) ); ?>"/></a>
	<h2><a href="<?php the_field('second_quad_link_target');?>"><?php the_field('second_quad_title');?></a></h2>
	<p style="text-align:left;margin:0 1em;"><?php the_field('second_quad_excerpt');?></p>
</li>
<li><a href="<?php the_field('third_quad_link_target');?>"><img src="<?php echo esc_url( get_field( 'third_quad_image' ) ); ?>"/></a>
	<h2><a href="<?php the_field('third_quad_link_target');?>"><?php the_field('third_quad_title');?></a></h2>
	<p style="text-align:left;margin:0 1em;"><?php the_field('third_quad_excerpt');?></p>
</li>
<li><a href="<?php the_field('fourth_quad_link_target');?>"><img src="<?php echo esc_url( get_field( 'fourth_quad_image' ) ); ?>"/></a>
	<h2><a href="<?php the_field('fourth_quad_link_target');?>"><?php the_field('fourth_quad_title');?></a></h2>
	<p style="text-align:left;margin:0 1em;"><?php the_field('fourth_quad_excerpt');?></p>
</li>
</ul>
</div><!-- ends quad page -->	

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
