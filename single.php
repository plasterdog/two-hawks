<?php
/**
 * The Template for displaying all single posts.
 *
 * @package plasterdog
 */

get_header(); ?>
<div id="hero-top">
<img src="<?php echo get_stylesheet_directory_uri() ?>/images/default-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
</div>  

    <div class="clear"></div>

    <div id="page" class="hfeed site">
  <div id="content" class="site-content" >
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
  <div class="entry-meta"></div><!-- .entry-meta -->
  </header><!-- .entry-header -->
  <div class="entry-content">
    <h1 class="responsive-page-title"><?php the_title(); ?></h1>
    <?php the_content(); ?>


  </div><!-- .entry-content -->

  <footer class="entry-footer">
    

    <?php edit_post_link( __( 'Edit', 'plasterdog' ), '<span class="edit-link">', '</span>' ); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-## -->

        <div class="clear"><!-- variation from default nav which restricts navigation within category -->
<div class="left-split-nav"><?php //previous_post_link('%link', '&larr; %title', TRUE) ?></div>
<div class="right-split-nav"><?php //next_post_link('%link', '%title &rarr;', TRUE) ?></div>
</div>

      <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() ) :
          comments_template();
        endif;
      ?>

    <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

  <div id="secondary" class="widget-area" role="complementary">
  <header class="page-header">  <h1 class="page-title"><?php the_title(); ?></h1></header>
  <hr/>
    <?php if(get_field('alternate_sidebar_content')) {?>
      <?php the_field('alternate_sidebar_content'); ?>
    <?php } ?><!-- ends the first condition -->
    <?php if(!get_field('alternate_sidebar_content')) {?>
      <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
      <?php endif; // end sidebar widget area ?>
    <?php }?> <!-- ends the second outer condition -->

  </div><!-- #secondary -->

<?php get_footer(); ?>