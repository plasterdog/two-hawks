<?php
/**
 * Template Name: Section Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package plasterdog
 */

get_header(); ?>
<?php if( get_field('hero_check_off') == 'show' ): ?>
	<div id="hero-top">
	<?php if ( get_field( 'page_hero_image' ) ): ?>		
	<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
	<?php else : ?>
	<img src="<?php echo get_stylesheet_directory_uri() ?>/images/default-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
	<?php endif; ?>
	</div>	
<?php endif; ?>

<?php if( get_field('hero_check_off') == 'hide' ): ?>
	<div id="hero-top"></div>
<?php endif; ?>	
		<div class="clear"></div>

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				
			</header><!-- .entry-header -->

			<div class="entry-content">
				
				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'plasterdog' ),
						'after'  => '</div>',
					) );
				?>
	<div>	



			</div><!-- .entry-content -->
	

	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
<!-- THIS IS THE REPEATER FIELD FOR SECTIONS -->
		<?php
		// check if the repeater field has rows of data
		if( have_rows('focus_sections') ): ?>
		<?php 	// loop through the rows of data
		    while ( have_rows('focus_sections') ) : the_row(); ?>
				<div class="section-clear"> 
				<div class="archive_left_picture"><img src="<?php the_sub_field('focus_thumbnail');?> "/>
				</div><!-- ends left picture -->
		<div class="archive_right_text">
		<h2><?php the_sub_field('focus_title');?></h2>
		<?php the_sub_field('focus_content');?> 
		<?php if(get_sub_field('more_focus_link_target')) {?>
		<p class="anchor-detail-link">	<a href="<?php the_sub_field('more_focus_link_target');?>"><?php the_sub_field('more_focus_label');?></a></p>
		<?php } ?><!-- ends the first condition -->
		<?php if(!get_sub_field('more_focus_link_target')) {?>			
		<?php }?> <!-- ends the second outer condition -->	
		</div><!-- ends right text -->
		</div><!--ends section wrapper -->
		<?php    endwhile;
		else :
		    // no rows found
		endif;
		?>


			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>

