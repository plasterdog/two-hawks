<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package plasterdog
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117929865-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117929865-1');
</script>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/font-awesome/css/font-awesome.min.css">
<!--[if lt IE 9]>
<script>
  var e = ("abbr,article,aside,audio,canvas,datalist,details," +
    "figure,footer,header,hgroup,mark,menu,meter,nav,output," +
    "progress,section,time,video,main").split(',');
  for (var i = 0; i < e.length; i++) {
    document.createElement(e[i]);
  }
</script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
<?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>	
<div id="full-top" style="background-image:url(<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>);  background-repeat:no-repeat;  background-position:center top; background-size:cover;">
<?php else : ?>
<div id="full-top">
<?php endif; ?>	
	<div id="upper-band">



<div class="masthead-holder">
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
	<nav id="site-navigation" class="main-navigation" role="navigation">
		<!-- THIS IS WHERE YOU SET THE MENU TOGGLE TEXT -->
			<div class="masthead-holder">
			<h1 class="menu-toggle"><?php _e( 'menu', 'plasterdog' ); ?></h1>
			<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'plasterdog' ); ?></a>

			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</div>
		</nav><!-- #site-navigation -->

<!-- inserts the header image-->
	<div id="left-head-1">

						<?php $header_image = get_header_image();
						if ( ! empty( $header_image ) ) { ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" />
						</a>
						<?php } // if ( ! empty( $header_image ) ) ?>

	</div><!-- ends left head 1 -->
<div id="left-head">

	<div id="left-head-2">
		<h1 class="site-title" ><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" style="color:#<?php header_textcolor(); ?>;"><?php bloginfo( 'name' ); ?></a></h1>
	<h2 class="masthead-info" style="color:#<?php header_textcolor(); ?>;"><?php bloginfo( 'description' ); ?></h2>
</div><!-- endfs left head 2-->
</div><!-- end left head-->
<div id="right-head">


			<div class="social">
				<?php if(get_option('pdog_phone') && get_option('pdog_phone') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_phone') ?></span>  <?php } ?>	
			<?php if(!get_option('pdog_phone')) {?>	&nbsp;		<?php }?>
				<ul class="top-social-icons">

					<?php if(get_option('pdog_facebook') && get_option('pdog_facebook') != '') {?>
					<li><a href="<?php echo get_option('pdog_facebook') ?>" target="_blank" ><i class="fa fa-facebook"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_linkedin') && get_option('pdog_linkedin') != '') {?>
					<li><a href="<?php echo get_option('pdog_linkedin') ?>" target="_blank"><i class="fa fa-linkedin"></i></a>	</li><?php } ?>	
						
					<?php if(get_option('pdog_googleplus') && get_option('pdog_googleplus') != '') {?>
					<li><a href="<?php echo get_option('pdog_googleplus') ?>" target="_blank"><i class="fa fa-google-plus"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_twitter') && get_option('pdog_twitter') != '') {?>
					<li><a href="<?php echo get_option('pdog_twitter') ?>" target="_blank"><i class="fa fa-twitter"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_instagram') && get_option('pdog_instagram') != '') {?>
					<li><a href="<?php echo get_option('pdog_instagram') ?>" target="_blank"><i class="fa fa-instagram"></i></a>	</li><?php } ?>	
					
					<?php if(get_option('pdog_youtube') && get_option('pdog_youtube') != '') {?>
					<li><a href="<?php echo get_option('pdog_youtube') ?>" target="_blank"><i class="fa fa-youtube-square"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_vimeo') && get_option('pdog_vimeo') != '') {?>
					<li><a href="<?php echo get_option('pdog_vimeo') ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_email') && get_option('pdog_email') != '') {?>
					<li><a href="mailto:<?php echo get_option('pdog_email') ?>" target="_blank"><i class="fa fa-envelope-o"></i></a>	</li><?php } ?>	
				</ul>
			</div><!-- ends social -->
		</div><!--ends right head -->			





				</div><!-- ends site branding -->
	</header><!-- ends masthead -->
</div><!-- ends masthead holder -->


</div><!-- ends upper band-->
	</header><!-- #masthead -->

</div> <!-- ends full top -->


