<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package plasterdog
 */

get_header(); ?>
<div id="hero-top">
<img src="<?php echo get_stylesheet_directory_uri() ?>/images/default-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
</div>
		<div class="clear"></div>

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

	<h1 class="responsive-page-title"><?php the_title(); ?></h1>	

			<?php /* Start the Loop */ ?>
			<ul class="just-archive-array">
			<?php while ( have_posts() ) : the_post(); ?>

<!-- POST FIELDS-->

<li>


	
<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
<div class="entry-content">  
	<div class="archive_left_picture">	
	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
	</div><!-- ends left picture -->
		<div class="archive_right_text">
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
		</header><!-- .entry-header -->
		<?php the_excerpt(); ?>
		<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the full article</a></p>
		</div><!-- ends right text -->
	</div>	<!-- ends entry content -->
</article><!-- #post-## -->
<div class="clear"></div>


<?php   } else { ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  

	<div class="entry-content">
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
		</header><!-- .entry-header -->
		<?php the_excerpt(); ?>
		<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the full article</a></p>
	</div><!-- .entry-content -->
 <?php    } ?>

</article><!-- #post-## -->
<div class="clear"><hr/></div>
</li>
	

	<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
	
	<?php endif; ?>

<!-- ENDS POST FIELDS -->

			<?php endwhile; ?>
</ul><!-- ends archive array-->
			<?php plasterdog_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

	<div id="secondary" class="widget-area" role="complementary">
		<header class="page-header">
				<h1 class="page-title">
					<?php
						if ( is_category() ) :
							single_cat_title();

						elseif ( is_tag() ) :
							single_tag_title();

						elseif ( is_author() ) :
							printf( __( 'Author: %s', 'plasterdog' ), '<span class="vcard">' . get_the_author() . '</span>' );

						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'plasterdog' ), '<span>' . get_the_date() . '</span>' );

						elseif ( is_month() ) :
							printf( __( 'Month: %s', 'plasterdog' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'plasterdog' ) ) . '</span>' );

						elseif ( is_year() ) :
							printf( __( 'Year: %s', 'plasterdog' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'plasterdog' ) ) . '</span>' );

						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'plasterdog' );

						elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
							_e( 'Galleries', 'plasterdog');

						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'plasterdog');

						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'plasterdog' );

						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'plasterdog' );

						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'plasterdog' );

						elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
							_e( 'Statuses', 'plasterdog' );

						elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
							_e( 'Audios', 'plasterdog' );

						elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
							_e( 'Chats', 'plasterdog' );

						else :
							_e( 'Archives', 'plasterdog' );

						endif;
					?>
				</h1>
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?><hr/>
			</header><!-- .page-header -->
		
		<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

			<aside id="search" class="widget widget_search">
				<?php get_search_form(); ?>
			</aside>

		<?php endif; // end sidebar widget area ?>
	</div><!-- #secondary -->

<?php get_footer(); ?>
